export class Meeting
{
    constructor
    (
        public id:number,
        public m_location:string,
        public m_date:string,
        public m_time:string,
        public topics:string[]
    ){};
}