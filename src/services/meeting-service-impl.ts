import { MeetingDAO } from "../daos/meeting-dao";
import { MeetingDAOImpl } from "../daos/meeting-dao-impl";
import { Meeting } from "../entities";
import { MissingResourceError } from "../errors";
import MeetingService from "./meeting-service";

export class MeetingServiceImpl implements MeetingService
{

    meetingDAO: MeetingDAO = new MeetingDAOImpl();

    registerMeeting(meeting: Meeting): Promise<Meeting>
    {
        return this.meetingDAO.createMeeting(meeting);
    }

    retrieveAllMeetings(): Promise<Meeting[]>
    {
        return this.meetingDAO.getAllMeetings();
    }

    retrieveMeetingByID(id: number): Promise<Meeting>
    {
        try
        {
            return this.meetingDAO.getMeetingByID(id);
        }
        catch (error)
        {
            throw new MissingResourceError(`Could not find meeting with ID ${id}`);
        }
    }

    async modifyMeeting(meeting: Meeting): Promise<Meeting>
    {
        try
        {
            const check = await this.meetingDAO.getMeetingByID(meeting.id);
            return this.meetingDAO.updateMeeting(meeting);
        }
        catch(error)
        {
            throw new MissingResourceError(`Could not find meeting with ID ${meeting.id}`);
        }
    }

    async removeMeetingByID(id: number): Promise<boolean>
    {
        try
        {
            const check = await this.meetingDAO.getMeetingByID(id);
            return this.meetingDAO.deleteMeetingByID(id);
        }
        catch(error)
        {
            throw new MissingResourceError(`Could not find meeting with ID ${id}`);
        }
    }
}