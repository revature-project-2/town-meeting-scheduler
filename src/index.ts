import cors from 'cors';
import express from 'express';
import { Meeting } from './entities';
import { MissingResourceError } from './errors';
import MeetingService from './services/meeting-service';
import { MeetingServiceImpl } from './services/meeting-service-impl';

const app = express();
app.use(express.json());
app.use(cors());

const meetingService:MeetingService = new MeetingServiceImpl();

app.get('/meetings', async (req, res) =>
{
    const meetings:Meeting[] = await meetingService.retrieveAllMeetings();
    res.status(200).send(meetings);
    return;
});

app.get('/meetings/:id', async (req, res) =>
{
    try
    {
        const meeting:Meeting = await meetingService.retrieveMeetingByID(Number(req.params.id));
        res.status(200).send(meeting);
        return;
    }
    catch (error)
    {
        if (error instanceof MissingResourceError)
        {
            console.log(JSON.stringify(error));
            res.status(404).send(error);
            return;
        }
    }
});

app.post('/meetings', async (req, res) =>
{
    if (req.headers["authorization"] === "p")
    {
        const meeting:Meeting = await meetingService.registerMeeting(req.body);
        console.log(`Created new meeting: ${JSON.stringify(meeting)}`);
        res.status(201).send(meeting);
        return;
    }
    else
    {
        console.log("A user attempted to create a meeting with an invalid password.");
        res.status(400).send("invalid header");
        return;
    }
});

app.put('/meetings/:id', async (req, res) =>
{
    try
    {
        if (req.headers["authorization"] === "p")
        {
            const meeting:Meeting = await meetingService.modifyMeeting(req.body);
            console.log(`Updated meeting with id ${meeting.id}`);
            res.status(200).send(meeting);
            return;
        }
        else
        {
            console.log("A user attempted to edit a meeting with an invalid password.");
            res.status(400).send("invalid header");
            return;
        }
    }
    catch (error)
    {
        if (error instanceof MissingResourceError)
        {
            console.log(JSON.stringify(error));
            res.status(404).send(error);
            return;
        }
    }
});

app.delete('/meetings/:id', async (req, res) =>
{
    try
    {
        if (req.headers["authorization"] === "p")
        {
            const meeting:boolean = await meetingService.removeMeetingByID(Number(req.params.id));
            res.status(205).send(meeting);
            return;
        }
        else
        {
            console.log("A user attempted to delete a meeting with an invalid password.");
            res.status(400).send("invalid header");
            return;
        }
    }
    catch (error)
    {
        if (error instanceof MissingResourceError)
        {
            console.log(JSON.stringify(error));
            res.status(404).send(error);
            return;
        }
    }
});

const PORT = process.env.PORT || 3000;
app.listen(PORT, ()=>console.log("Application started"));