# Issue Review Board

## Project Description
The Issue Review board is an application for a small local town. The application allows constituents to submit issues that they have noticed in the town. The review board members can look over these issues. Board members can then create meetings which can be viewed by constituents and display information on what needs to be discussed. The application is a microservices architecture that is entirely container based. The application ingests information from GCP pub/sub and uses GKE and Cloud Run for hosting the services.

## Technologies Used
- NodeJS - Version 13
- React - Version 17
- Express - Version 4.17.1
- Axios - Version 0.22.0
- TypeScript - Version 4.4.3
- MaterialUI - Version 5.0.1
- Bunyan - Version 1.8.15
- @google-cloud/logging-bunyan - Version 3.1.1
- BigQuery - Version 2.28.0
- Docker - Version 20.10.7
- Terraform - Version 1.0.8
- Kubernetes - Version 1.22.2

## Features
- View issues submitted by citizens.
- Mark issues as reviewed or highlight them.
- Filter issues by type, date of posting, reviewed, or highlighted.
- Create new issues.
- Make new meetings.
- Edit/delete meetings.

## Getting Started

### Frontend
```bash
git clone https://gitlab.com/revature-project-2/town-hall-service-frontend.git
```
### Meeting Scheduler REST API
```bash
git clone https://gitlab.com/revature-project-2/town-meeting-scheduler.git
```
### Issue Ingestion Service
```bash
git clone https://gitlab.com/revature-project-2/issue-ingestion.git
```
### Issue Analysis Service
```bash
git clone https://gitlab.com/revature-project-2/issue-analysis-service.git
```

You will need to set up a PostgreSQL database with some data in it. Simply change the connection credentials in the connection.ts file of the Meeting Scheduler Service.
A Datastore database is needed to store issues.
If you haven't already, [install NodeJS](https://nodejs.org/en/download/).
Run 'npm install' on each directory.
Open a terminal in each directory and type "npm start" then press enter.
In the web browser add "/home" to the url to get to the home page.

## Usage
- Once the page is loaded, the user will be at the /home route. From here, you can select "View Issues" to view all the posted issues.
- The user can create an issue by selecting the "Create Issue" button on the navigational side-bar. Note: the date of posting is automatically included when the user submits the issue.
- A council member can mark the issue as reviewed, or highlight it, by selecting the appropriate check box on the card displaying the issue.
- Council members can create a meeting to include the issue in the "Create Meeting" button on the side-bar. These meetings can only be created, deleted, or edited by council members.
- A user is considered to be a council member if they have the necessary password, which will be requested upon attempting to create, edit, or delete meetings.

## Contributors
Thomas Smithey, Alejandra Cajiao, Brystan Baeder, Phong Vo