import { connect } from "../src/connection";

// Test the connection to the instance on GCP.
test("Should create a connection", async () =>
{
    const result = await connect.query("select * from meeting");
    expect(result).toBeTruthy();
});

afterAll(async () =>
{
    connect.end();
});