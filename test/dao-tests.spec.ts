import { connect } from "../src/connection";
import { MeetingDAO } from "../src/daos/meeting-dao";
import { MeetingDAOImpl } from "../src/daos/meeting-dao-impl";
import { Meeting } from "../src/entities";

/**
 * A series of tests made to veryify that we can perform CRUD
 * operations with our DAO implementation's functions.
 */

const meetingDao:MeetingDAO = new MeetingDAOImpl();
const time = new Date().toLocaleTimeString("en-US");
const date = new Date().toDateString();

test("Create a meeting", async () =>
{
    const testMeeting = new Meeting(1, "Dallas, TX", date, time, ["Safety", "Pollution"]);
    const result = await meetingDao.createMeeting(testMeeting);
    expect(result.id).not.toBe(0);
});

test("Get all meetings", async () =>
{
    const m1:Meeting = new Meeting(1, "Gainesville, FL", date, time, ["Safety", "Pollution"]);
    const m2:Meeting = new Meeting(1, "Chicago, IL", date, time, ["Safety", "Pollution"]);
    const m3:Meeting = new Meeting(1, "New York, NY", date, time, ["Safety", "Pollution"]);

    await meetingDao.createMeeting(m1);
    await meetingDao.createMeeting(m2);
    await meetingDao.createMeeting(m3);

    const meetings:Meeting[] = await meetingDao.getAllMeetings();
    expect(meetings.length).toBeGreaterThanOrEqual(3);
});

test("Get meeting by id", async () =>
{
    let meeting:Meeting = new Meeting(1, "Boston, MA", date, time, ["Safety", "Pollution"]);
    meeting = await meetingDao.createMeeting(meeting);
    let retrievedMeeting:Meeting = await meetingDao.getMeetingByID(meeting.id);
    expect(retrievedMeeting.id).toBe(meeting.id);
});

test("Update Meeting", async () =>
{
    let meeting:Meeting = new Meeting(1, "Gainesville, FL", date, time, ["Safety", "Pollution"]);
    meeting = await meetingDao.createMeeting(meeting);
    meeting.m_location = "Kansas City, MO";
    meeting = await meetingDao.updateMeeting(meeting);
    expect(meeting.m_location).toBe("Kansas City, MO");
});

test("Delete meeting by id", async () =>
{
    let meeting:Meeting = new Meeting(1, "Gainesville, FL", date, time, ["Safety", "Pollution"]);
    meeting = await meetingDao.createMeeting(meeting);
    const result:boolean = await meetingDao.deleteMeetingByID(meeting.id);
    expect(result).toBeTruthy();
});

afterAll(async () =>
{
    connect.end();
});